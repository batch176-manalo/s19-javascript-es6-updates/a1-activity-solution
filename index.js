// alert("hey");

// cube
let num = 2;
let getCube = num ** 3;
let message = `The cube of ${num} is ${getCube}`;
console.log(message);

// address
let address = [258, "Washington Ave NW", "California", "90011"];
const [houseNum, street, state, zipCode] = address;
message = `I live at ${houseNum} ${street}, ${state} ${zipCode}`;
console.log(message);

// animal
const animal = {
		name: "Lolong",
		type: "saltwater crocodile",
		weight: "1075 kgs",
		measurement: "20 ft 3 in"
	}

const {name, type, weight, measurement} = animal;
message = `${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}.`
console.log(message);

// number
let numbers = [1, 2, 3, 4, 5];

numbers.forEach((num) => console.log(num));

// dog
class Dog {
	constructor(name, age, breed) {
		this.name = name,
		this.age = age,
		this.breed = breed
	}
}

const myDog = new Dog (`Frankie`, 5, `Mianiture Dachshund`);
console.log(myDog);